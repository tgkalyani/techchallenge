﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SearchTitles.HelperClasses;
using SearchTitles.Models;

namespace SearchTitles.Controllers
{
    public class TitleDetailsController : ApiController
    {
        public TitleDetails GetTitleDetails([FromUri] int? titleid)
        {
            GetTitleDetailHelper getdetails = new GetTitleDetailHelper();
            TitleDetails td = new TitleDetails();
            td = getdetails.returnTitleDetails(Convert.ToInt32(titleid));
            return td;

        }
    }
}
