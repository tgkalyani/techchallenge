﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SearchTitles.HelperClasses;


namespace SearchTitles.Controllers
{
    public class SearchController : ApiController
    {
        public List<TitleSearchClass> GetSearchResults([FromUri] string searchkey)
        {
            SearchResult sr = new SearchResult();
            List<TitleSearchClass> results = null;
            results =  sr.ListOfMatchedNames(searchkey); 
            return results;
        }

    }
}
