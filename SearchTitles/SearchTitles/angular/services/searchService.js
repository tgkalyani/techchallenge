﻿titleSearch.service('searchService', function ($resource, $q) {

    var getSearchedTitles = function (searckey) {
        var searchResource = $resource('api/Search');
        var deferred = $q.defer();
        searchResource.get({ searchkey: searckey }).$promise
        .then(function (searchResourceData) {
            deferred.resolve(searchResourceData);
        });

        return deferred.promise;
    };

    return {
        getSearchedTitles: getSearchedTitles
    };
});