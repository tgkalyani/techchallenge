﻿titleSearch.factory('WebAPI', function ($http) {
    return {
        GET: function (apiURL, params, callback) {           
            $http({ url: apiURL, method: "GET", params: params})
            .success(function (data) {
                callback(data);
            });
        },
        POST: function (apiURL, params, callback) {
            $http({ url: apiURL, method: "POST", params: params})
            .success(function (data) {
                callback(data);
            });
        }
    };
});