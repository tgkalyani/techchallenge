﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TechChanllenge.DataAccess;

namespace SearchTitles.HelperClasses
{   
    public class SearchResult
    {
        TitlesEntities titledb = new TitlesEntities();

        public List<TitleSearchClass> ListOfMatchedNames(string searchkey)
        {

            var matches = titledb.Titles.Where(m => m.TitleName.Contains(searchkey)).Select(i => new TitleSearchClass
            {
                TitleName = i.TitleName,
                TitleId = i.TitleId
            });
            return matches.ToList();
        }

    }
}