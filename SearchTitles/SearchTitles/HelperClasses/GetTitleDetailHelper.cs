﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TechChanllenge.DataAccess;
using SearchTitles.Models;

namespace SearchTitles.HelperClasses
{
    public class GetTitleDetailHelper
    {

        TitlesEntities titledb = new TitlesEntities();     


        public List<LanguageforTitle> GetTitleLanguages(int titleid)
        {
            var languages = titledb.Titles.
                 Join(titledb.OtherNames,
                 o => o.TitleId, od => od.TitleId,
                 (t, td) => new { t, td })
                 .Where(m => m.t.TitleId == titleid)
                 .Select(m => new LanguageforTitle
                 {                     
                     TitleLanguage = m.td.TitleNameLanguage
                 });
           var  languageslist = languages.ToList();
            return languages.ToList();
        }

        public List<GenreforTitle> GetTitleGenre(int titleid)
        {
            var genres = titledb.Titles.
                 Join(titledb.TitleGenres,
                 t => t.TitleId, tg => tg.TitleId,
                 (t, td) => new { t, td })
                 .Where(m => m.t.TitleId == titleid)
                 .Join(titledb.Genres,
                 tg => tg.td.GenreId, g => g.Id, (tg, g) => new { tg, g })
                 .Where(gn => gn.tg.t.TitleId == titleid)
                 .Select(m => new GenreforTitle
                 {
                     TitleGenre = m.g.Name

                 });
                 

            return genres.ToList();
        }

        public List<StoryLineDetails> GetTitleStoryLine(int titleid)
        {
            var storyline = titledb.Titles.
                 Join(titledb.StoryLines,
                 t => t.TitleId, sl => sl.TitleId,
                 (t, sl) => new { t, sl })
                 .Where(m => m.t.TitleId == titleid)
                 .Select(m => new StoryLineDetails
                 {
                     storyLineType = m.sl.Type,
                     storyLineDescription = m.sl.Description
                 });

            return storyline.ToList();
        }

        public List<AwardDetails> GetTitleAward(int titleid){

            var award = titledb.Titles.
                Join(titledb.Awards,
                t => t.TitleId, a => a.TitleId,
                (t, a) => new { t, a })
                .Where(m => m.t.TitleId == titleid)
                .Select(m => new AwardDetails
                {
                    awardName = m.a.Award1,
                    yearWon = m.a.AwardYear,
                    awardCompany = m.a.AwardCompany
                }).OrderBy(u=>u.yearWon);

            return award.ToList();

        }

        public List<ParticipantDetails> GetTitleParticipants(int titleid)
        {
            var participants = titledb.Titles.
                 Join(titledb.TitleParticipants,
                 t => t.TitleId, tp => tp.TitleId,
                 (t, tp) => new { t, tp })
                 .Join(titledb.Participants,
                 tg => tg.tp.ParticipantId, p => p.Id, (tg, p) => new { tg, p })
                 .Where(gn => gn.tg.t.TitleId == titleid)
                 .Select(m => new ParticipantDetails
                 {
                     participantName = m.p.Name,
                     participantRoleName = m.tg.tp.RoleType

                 }).OrderBy(u=>u.participantRoleName);


            return participants.ToList();
        }



        public TitleDetails returnTitleDetails(int titleid)
        {
            TitleDetails titdetails = new TitleDetails();
            titdetails.Languages = GetTitleLanguages(titleid);
            titdetails.Genere = GetTitleGenre(titleid);
            titdetails.StoryLine = GetTitleStoryLine(titleid);
            titdetails.Award = GetTitleAward(titleid);
            titdetails.Participant = GetTitleParticipants(titleid);
            return titdetails;
        }



    }
}