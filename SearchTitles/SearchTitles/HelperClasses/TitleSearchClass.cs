﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SearchTitles.HelperClasses
{
   public class TitleSearchClass
    {
        public string TitleName { get; set; }

        public int TitleId { get; set; }
    }
}
