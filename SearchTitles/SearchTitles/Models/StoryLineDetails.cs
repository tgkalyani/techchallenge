﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SearchTitles.Models
{
    public class StoryLineDetails
    {
        public string storyLineType { get; set; }
        public string storyLineDescription { get; set; }
    }
}