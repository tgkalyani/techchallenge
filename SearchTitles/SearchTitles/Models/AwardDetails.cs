﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SearchTitles.Models
{
    public class AwardDetails
    {
        public string awardName { get; set; }
        public int? yearWon { get; set; }
        public string awardCompany { get; set; }


    }
}