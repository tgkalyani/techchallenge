﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SearchTitles.Models
{
    public class ParticipantDetails
    {
        public string participantName { get; set; }
        public string participantRoleName { get; set; }
    }
}