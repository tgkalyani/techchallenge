﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TechChanllenge.DataAccess;
using SearchTitles.HelperClasses;

namespace SearchTitles.Models
{
    public class TitleDetails
    {
        public List<LanguageforTitle> Languages { get; set; }
        public List<GenreforTitle> Genere { get; set; }
        public List<StoryLineDetails> StoryLine { get; set; }
        public List<AwardDetails> Award { get; set; }
        public List<ParticipantDetails> Participant { get; set; }      
    }
}